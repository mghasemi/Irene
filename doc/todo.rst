=============================
To Do
=============================

Based on the current implementation, the followings seems to be implemented/modified:

	+ Reduce dependency on SymPy.
	+ Keep track of original expressions before reduction.
	+ Write a LaTeX method.
	+ Include sdp solvers installation (subject to copyright limitations).
	+ Error handling for CSDP and SDPA failure.

Done
==================

The following to-dos were implemented:

	+ Extract solutions (at least for polynomials)- in v.1.1.0.
	+ SOS decomposition- in v.1.1.0.
	+ Write a ``__str__`` method for ``SDPRelaxations`` printing- in v.1.1.0.
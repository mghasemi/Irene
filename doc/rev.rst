=============================
Revision History
=============================

**Version 1.1.0 (December 25, 2016- Merry Christmas)**

	- Extracting minimizers via ``SDRelaxSol.ExtractSolution()`` and help of ``scipy``,
	- Extracting minimizers implementing Lasserre-Henrion algorithm,
	- Adding ``SDPRelaxations.Probability`` and ``SDPRelaxations.PSDMoment`` to give more flexibility over moments and enables rational minimization.
	- SOS decomposition implemented.
	- ``__str__`` method for ``SDPRelaxations``.
	- Using `pyOpt` as the external optimizer.
	- More benchmark examples.

**Version 1.0.0 (Dec 07, 2016)**
	
	- Initial release (Irene's birthday)